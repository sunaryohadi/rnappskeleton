# React Native App Skeleton #

This is base skeleton for react native app development

### What is this repository for? ###

* React Native development using React Native Router Flux and Redux
* v0.1
* Redux based on [Tackling React Native navigation with React Native Router Flux and Redux](https://medium.com/@bosung90/tackling-react-native-navigation-with-react-native-router-flux-and-redux-27fcd86fb2b7#.1hlmrx5qc)

### Install ###

* cd dir_to/RNAppSkeleton
* npm install
* react-native link
* react-native run-ios


### Contact ###

* Agensi Digital team