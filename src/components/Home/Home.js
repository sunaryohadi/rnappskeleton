/* ========================================================
 *  Home
 * -------------------------------------------------------- */
import React, { Component } from 'react';
import { Container, Content, View, Text } from 'native-base';
import { AppHeader, AppFooter } from '../common';

export default class Home extends Component {

	componentWillMount() {
		this.getNews();
	}

	async getNews() {
		// console.log(global.accessToken);
		this.setState({showProgress: true})
		try {   
			let response = await fetch( 'https://tihotline.com/m/home' , {
					method: 'POST',
					headers: {
					  'Accept': 'application/json',
					  'Content-Type': 'application/json',
					},
				  });

			let res = await response.json();
			if (response.status >= 200 && response.status < 300) {
				// this.setState({list: res}); 
				console.log(res);
			} else {
				//Handle error
				let error = res;
				throw error;
			}
		  
		} catch(error) {
			this.setState({error: error });
			console.log( error );
		} finally {
			this.setState({showProgress: false})
		}  
	}

	render() {
		const {count, scene, incrementCount, decrementCount, incrementCountThunk, handleCard} = this.props;
		return (
			<Container>
				<AppHeader { ...this.props} />
				<Content>
					<View style={{ padding: 10 }}>
					<Text>You are currently on {scene && scene.sceneKey}</Text>
					<Text>Count: {count}</Text>
						<View style={{ padding: 10 }}>
						<Text onPress={()=>incrementCount()}>Increment Count</Text>
						<Text onPress={()=>incrementCountThunk()}>Increment Count Thunk</Text>
						<Text onPress={()=>decrementCount()}>Decrement Count</Text>
					</View>
					<Text onPress={()=>handleCard()}>Push New Card Scene</Text>
					</View>

				</Content>
				<AppFooter { ...this.props} />
			</Container>
		);
	}
}