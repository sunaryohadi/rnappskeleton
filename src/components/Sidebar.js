import React, { Component } from 'react';
import { Container, Header, Content, List, ListItem, Icon,  Text } from 'native-base';

class Sidebar extends Component {

	render() {
		return (
			<Container style={{ backgroundColor: '#FFF'}}>
				<Header>
					<Text>Olelang</Text>
				</Header>
				<Content>
					<List>
						<ListItem button iconLeft onPress={ () => { console.log('Klik'); } } >
							<Icon name='ios-settings' />
							<Text style={{ marginLeft: 10 }}>Settings</Text>
						</ListItem>
					</List>
				</Content>
			</Container>
		);
	}

}

export default Sidebar;