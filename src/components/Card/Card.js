import React from 'react'
import { Container, Content, View, Text } from 'native-base';
import { AppHeader, AppFooter } from '../common';

export default (props) => {
  const {scene} = props

  props.title = 'Card';

  return (
	<Container>
		<AppHeader { ...props} />
		<Content>
			<View style={{flex:1, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
			  <Text>You are currently on {scene && scene.sceneKey}</Text>
			</View>
		</Content>
		<AppFooter { ...props} />
	</Container>	
  )
}