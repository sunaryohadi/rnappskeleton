/* ========================================================
 *  Header
 * -------------------------------------------------------- */
import React, { Component } from 'react';
import { Header, Body, Right, Left, Title, Icon, Button } from 'native-base';

const AppHeader = (props) => {

	return ( 
		<Header>
			<Left>
	            <Button transparent onPress={() => props.openDrawer() }>
	                <Icon name='menu' />
	            </Button>
	        </Left>
	  		<Body>
	  			<Title>{ props.title } </Title>
	  		</Body>	
			<Right>
	            <Button transparent>
					<Icon name="person" />
				</Button>
				<Button transparent>
					<Icon name="log-in" />
				</Button>
			</Right>
		</Header>	
	);
	
}

// Make the component available to other parts of the app
export { AppHeader };