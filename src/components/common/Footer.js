/* ========================================================
 *  Footer
 * -------------------------------------------------------- */
import React, { Component } from 'react';
// import { StyleSheet,Text, View } from 'react-native';
import { Footer, FooterTab, Icon, Badge, Button, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';


const AppFooter = (props) => {
	const { scene } = props;
	
	return ( 
		<Footer >
			<FooterTab>
				<Button active={scene && scene.sceneKey === 'home'} onPress={ () => Actions.home({ reset: true }) } >
					<Icon active name="apps" />
				</Button>
				<Button active={scene && scene.sceneKey === 'bid'} onPress={ () => Actions.bid({ reset: true }) } >
					<Icon name="locate" />
					<Text style={styles.text}>Bid</Text>
				</Button>
				<Button active={scene && scene.sceneKey === 'sell'} onPress={ () => Actions.sell({ reset: true }) } >
					<Icon name="navigate" />
					<Text style={styles.text}>Sell</Text>
				</Button>
				<Button active={scene && scene.sceneKey === 'chat'} onPress={ () => Actions.chat({ reset: true }) } >
					<Icon name="chatbubbles" />
					<Text style={styles.text}>Chat</Text>
				</Button>
				<Button active={scene && scene.sceneKey === 'notifcation'} onPress={ () => Actions.chat({ reset: true }) } >
					<Icon name="notifications" />
					<Text style={styles.text}>Notif</Text>
				</Button>
			</FooterTab>
		</Footer>
	);
	
}

const styles = {
	text : {
		fontSize: 12
	}
}

// Make the component available to other parts of the app
export { AppFooter };