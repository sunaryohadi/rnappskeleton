// Redux implementation reference: 
// - https://medium.com/@bosung90/tackling-react-native-navigation-with-react-native-router-flux-and-redux-27fcd86fb2b7#.jhhb22ij9
// - https://github.com/bosung90/react-native-router-flux-example
import React from 'react';
import { Router, Scene, Actions, Modal, ActionConst  } from 'react-native-router-flux';
import { Provider, connect } from 'react-redux'
import configureStore from './store/configureStore'
import { Drawer, Text } from 'native-base';
import Sidebar from './components/Sidebar';
import Home  from './components/Home';
import Card from './components/Card';

const scenes = Actions.create(
	<Scene key="modal" component={Modal} >
		<Scene key="root"  hideNavBar={true}>
			<Scene key="home" component={Home} title="Home"  type={ActionConst.REPLACE} />
			<Scene key="bid" component={Card} title="Bid"  type={ActionConst.REPLACE} />
			<Scene key="sell" component={Card} title="Sell"  type={ActionConst.REPLACE} />
			<Scene key="chat" component={Card} title="Chat"  type={ActionConst.REPLACE} />
			<Scene key='card' component={Card} type={ActionConst.REPLACE} />
		</Scene>
	</Scene>
);		

const ConnectedRouter = connect()(Router);
const store = configureStore();

const RouterComponent = () => {
	closeDrawer = () => {
		this._drawer._root.close()
	};
	openDrawer = () => {
		this._drawer._root.open()
	};
	return (
		<Provider store={store}>
			<Drawer
				ref={(ref) => { this._drawer = ref; }}
				content={<Sidebar navigator={this._navigator} />}
				onClose={() => this.closeDrawer()}
			>
				<ConnectedRouter scenes={scenes}  openDrawer={ () => this.openDrawer() } /> 
			</Drawer>
      	</Provider>
	);
};

// <Router scenes={scenes}  openDrawer={ () => this.openDrawer() }  />

export default RouterComponent;